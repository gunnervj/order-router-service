package com.codesummon.order.router.model;


import io.smallrye.config.ConfigMapping;

import java.util.List;

@ConfigMapping(prefix = "app")
public interface AppConfig {
    Zookeeper zookeeper();
     List<RouteConfig> routeConfigs();

      interface RouteConfig {
          String routeName();
          Boolean active();
     }

     interface Zookeeper {
          String server();
          String root();
     }


}
