package com.codesummon.order.router;

import com.codesummon.order.router.model.AppConfig;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.zookeepermaster.MasterComponent;

@ApplicationScoped
@RequiredArgsConstructor
public class OrderRouter extends RouteBuilder {
    private final AppConfig appConfig;

    @Override
    public void configure() throws Exception {
        addZookeeperMaster();
        appConfig.routeConfigs().forEach(routeConfig -> {
            if (routeConfig.active()) {
                createRoute(routeConfig.routeName());
            }
        });
        from("direct:processOrder")
                .routeId("processOrderRoute")
                .log("Processing order: ${body}")
                .to("log:processedOrder");
    }

    private void addZookeeperMaster() {
        MasterComponent masterComponent = new MasterComponent();
        masterComponent.setZkRoot(appConfig.zookeeper().root());
        masterComponent.setZooKeeperUrl(appConfig.zookeeper().server());
        getContext().addComponent("zookeeper-master", masterComponent);
    }

    private void createRoute(String routeName) {
        from("zookeeper-master:orders:amqp:queue:" + routeName)
                .routeId(routeName)
                .log("Received order message from" + routeName + "  Route: ${body}")
                .to("direct:processOrder");
    }

}
